<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puestos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pue_nombre');
            $table->timestamps();
        });

        DB::table('puestos')->insert(
                        array(
                                array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA  DE ADMINISTRACION DE CAJA TESORERIA 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA CONTROL PREVIO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE  INVESTIGACION Y ENFOQUE POLITICO 2 ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE ANALISIS MEDIATICO 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE COMPRAS',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE COMUNICACION INTERINSTITUCIONAL 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE CONTENIDOS 1',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE CONTENIDOS DE MEDIOS INSTITUCIONALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE CONTENIDOS DE MEDIOS INSTITUCIONALES 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE CONTRATACION PUBLICA 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE DESARROLLO DE SOFTWARE 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE EDICION EL CIUDADANO 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE ENFOQUE POLITICO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE GESTION DE LA COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE IDENTIFICACION Y DESARROLLO DE PROCESOS 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE INFORMACION, SEGUIMIENTO Y EVALUACION 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE INNOVACION Y REDES DIGITALES 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE INVESTIGACION Y ENFOQUE POLITICO 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE LA DIRECCION ADMINISTRATIVA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE LA DIRECCION DE INFORMES GUBERNAMENTALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE LA DIRECCION NACIONAL DE GESTION DE LA COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE MEDIOS EL CIUDADANO 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE NOMINA Y REMUNERACIONES 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE PATROCINIO 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE PRENSA 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE PROCESOS ',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE PROCESOS Y GESTION DEL CAMBIO Y CULTURA ORGANIZACIONAL 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE PRODUCCION DE EVENTOS MARKETING Y PUBLICIDAD 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE PRODUCCION MULTIMEDIA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE PROMOCION Y REDES DIGITALES ',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE PROMOCION Y REDES DIGITALES 2 ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE RELACIONES PUBLICAS 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE SERVICIOS GENERALES 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE SINTESIS INFORMATIVA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE SINTESIS INFORMATIVA 1',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE SINTESIS INFORMATIVA 1 ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE TALENTO HUMANO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE TALENTO HUMANO 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DEL PROYECTO CIUDADANO TV',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA DEL PROYECTO RADIO CIUDADANA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ANALISTA FINANCIERO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASESOR 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASESOR 3',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASESOR 4',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASESOR 5',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE  RELACIONES PUBLICAS',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE CONTENIDOS DE MEDIOS INSTITUCIONALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE CONTRATACION PUBLICA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE EDICION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE GESTION DE LA COMUNICACION ',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE LA COORDINACION GENERAL  DE ASESORIA JURIDICA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE LA COORDINACION GENERAL ESTRATEGICA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE LA DIRECCION ADMINISTRATIVA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE LA DIRECCION DE INFORMES GUBERNAMENTALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE LA DIRECCION FINANCIERA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE LA DIRECCION ZONAL GUAYAQUIL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE LA SUBSECRETARIA DE MEDIOS INSTITUCIONALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE LA SUBSECRETARIA NACIONAL DE COMUNICACIÓN',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE MONITOREO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE PRENSA EXTERNA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE PRENSA PRESIDENCIA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE PRENSA PRESIDENCIAL ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE PRODUCCION DE CAMPO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE PRODUCCION Y MARKETING',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE PROMOCION DE LA COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE RELACIONES PUBLICAS',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE SINTESIS INFORMATIVA ',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE SOPORTE TECNICO Y CAPACITACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE TALENTO HUMANO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DEL PROYECTO CIUDADANO TV',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DEL PROYECTO CIUDADANO TV (CAMAROGRAFO)',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE DEL PROYECTO RADIO CIUDADANA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE PRENSA EXTERNA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ASISTENTE PROYECTO RADIO CIUDADANA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'AUXILIAR DE SERVICIOS',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'AUXILIAR DE SERVICIOS/GUARDIA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'CAMAROGRAFO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'CAMAROGRAFO ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'CHOFER',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'CONSERJE',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'COORDINADOR DE DESPACHO INSTITUCIONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'COORDINADOR GENERAL ADMINISTRATIVO FINANCIERO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'COORDINADOR GENERAL DE ASESORIA JURIDICA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'COORDINADOR GENERAL DE GESTION ESTRATEGICA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'COORDINADOR GENERAL DE PLANIFICACION E INVERSION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR DE INFORMACION, SEGUIMIENTO Y EVALUACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR DE PATROCINIO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR DE PLANIFICACION E INVERSION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR DE TALENTO HUMANO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR NACIONAL DE CONTENIDOS DE MEDIOS INSTITUCIONALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR NACIONAL DE ENFOQUE POLITICO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR NACIONAL DE INFORMES GUBERNAMENTALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR ADMINISTRATIVA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR DE ADMINISTRACION DE PROCESOS Y GESTION DEL CAMBIO Y CULTURA ORGANIZACIONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR DE GESTION DOCUMENTAL Y ARCHIVO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR DE ASESORIA LEGAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR FINANCIERA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR NACIONAL DE GESTION DE LA COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR NACIONAL DE PRODUCCION DE EVENTOS MARKETING Y PUBLICIDAD',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'DIRECTOR NACIONAL DE PROMOCION, INNOVACION Y REDES DIGITALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA 1 DE ENFOQUE POLITICO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA 1 PROYECTO CIUDADANO TV',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA 5 PROYECTO EL CIUDADANO TV',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA 5 PROYECTO RADIO CIUDADANA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE DESARROLLO DE SOFTWARE',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE  INGENIERIA DE REDES, SEGURIDADES Y COMUNICACIONES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE ADMINISTRACION DE CAJA TESORERIA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE ANALISIS INTERNACIONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE ANALISIS MEDIATICO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE ASESORIA LEGAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE AUDIOVISUALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE CAMBIO Y CULTURA ORGANIZACIONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE COMUNICACION INTERINSTITUCIONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE CONTABILIDAD',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE CONTENIDOS',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE CONTRATACIÓN PÚBLICA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE CONTROL PREVIO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE EL CIUDADANO IMPRESO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE GESTION DE LA COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE GESTION DOCUMENTAL Y ARCHIVO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => '',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE INFORMACION, SEGIMIENTO Y EVALUACION ',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE INFORMES GUBERNAMENTALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE INVESTIGACION Y ENFOQUE POLITICO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE LA DIRECCION DE INFORMES GUBERNAMENTALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE LOGISTICA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE PATROCINIO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE PLANIFICACION E INVERSION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE PRENSA EXTERNA ZONAL CUENCA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE PRESUPUESTO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE PROCESOS ADMINISTRATIVOS ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE PRODUCCION DE  EVENTOS',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE PRODUCCION DE CAMPO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE PRODUCCION DE EVENTOS MARKETING Y PUBLICIDAD',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE PROMOCION, INNOVACION Y REDES DIGITALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE RELACIONES PUBLICAS',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE SERVICIOS GENERALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE SINTESIS  INFORMATIVA ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE SINTESIS INFORMATIVA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE TALENTO HUMANO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACION ',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE TRANSMISIONES Y GESTION WEB',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DE TRANSPORTES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DEL PROGRAMA DOMINICAL EL CIUDADANO',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DEL PROYECTO CIUDADANO TV',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'ESPECIALISTA DEL PROYECTO RADIO CIUDADANA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'GERENTE DE PROYECTO 3',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'PERIODISTA DE INVESTIGACION POLITICA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'PERIODISTA DE SINTESIS INFORMATIVA 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'RECEPCIONISTA',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'SECRETARIA 1',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'SECRETARIO NACIONAL DE COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'SUBSECRETARIO DE PROMOCION DE LA COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'SUBSECRETARIO DE INFORMACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'SUBSECRETARIO DE MEDIOS INSTITUCIONALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'SUBSECRETARIO NACIONAL DE COMUNICACION',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'TECNICO ADMINISTRATIVO 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'TECNICO DE INFORMES GUBERNAMENTALES 2',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'TECNICO DE LOGISTICA Y AVANZADA 2 ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'TECNICO DE MEDIOS INSTITUCIONALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'TECNICO DE PROMOCION Y  REDES DIGITALES 1 ZONAL',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'TECNICO DE PROMOCION Y REDES DIGITALES 1',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'TECNICO DE SERVICIOS GENERALES',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'TECNICO DE TRANSPORTES 1',
                                        
                                ),array(
                                        'id' => '',
                                        'pue_nombre' => 'TECNICO WEB 2',
                                        
                                ),




                        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('puestos');
    }
}
