<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvinciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provincias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pro_nombre');
            $table->timestamps();
            //
        });


        DB::table('provincias')->insert(
                        array(
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Azuay',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Bolívar',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Cañar',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Carchi',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Cotopaxi',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Chimborazo',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'El Oro',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Esmeraldas',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Guayas',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Imbabura',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Los Ríos',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Manabí',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Morona Santiago',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Napo',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Pastaza',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Pichincha',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Tungurahua',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Zamora Chinchipe',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Galápagos',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Sucumbíos',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Orellana',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Santo Domingo de Los Tsáchilas',
                                        
                                ),
                                array(
                                        'id' => '',
                                        'pro_nombre' => 'Santa Elena',
                                        
                                ),  
                        ));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('provincias');
    }
}
