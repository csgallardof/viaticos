<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uni_nombre');
            $table->timestamps();
        });


        DB::table('unidades')->insert(
                        array(
                                array(
                                        'id' => '',
                                        'uni_nombre' => 'COORDINACION GENERAL ADMINISTRATIVA FINANCIERA',
                                ),
                                array(
                                        'id' => '',
                                        'uni_nombre' => 'COORDINACION GENERAL DE ASESORIA JURIDICA',
                                ),
                                array(
                                        'id' => '',
                                        'uni_nombre' => 'COORDINACION GENERAL DE GESTION ESTRATEGICA',
                                ),
                                array(
                                        'id' => '',
                                        'uni_nombre' => 'COORDINACION GENERAL DE PLANIFICACION E INVERSION',
                                ),
                                array(
                                        'id' => '',
                                        'uni_nombre' => 'DESPACHO SECOM',
                                ),
                                array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION  DE TALENTO HUMANO',
                                ),
                                array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION ADMINISTRATIVA',
                                ),
                                array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION DE ADMINISTRACION DE PROCESOS Y GESTION DEL CAMBIO Y CULTURA ORGANIZACIONAL',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION DE ASESORIA LEGAL',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION DE GESTION DOCUMENTAL Y ARCHIVO',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION DE INFORMACION, SEGUIMIENTO Y EVALUACION',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => '',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION DE PATROCINIO',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACION',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION FINANCIERA',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION GENERAL DE PLANIFICACION E INVERSION',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION NACIONAL DE CONTENIDOS DE MEDIOS INSTITUCIONALES',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION NACIONAL DE ENFOQUE POLITICO',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION NACIONAL DE GESTION DE LA COMUNICACION',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION NACIONAL DE INFORMES GUBERNAMENTALES',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION NACIONAL DE PRODUCCION DE EVENTOS MARKETING Y PUBLICIDAD',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION NACIONAL DE PROMOCION, INNOVACION Y REDES DIGITALES',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION NACIONAL DE SINTESIS Y ANALISIS INTERNACIONAL',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'DIRECCION ZONAL GUAYAQUIL',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'PROYECTO EL CIUDADANO TV',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'PROYECTO RADIO CIUDADANA',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'SUBSECRETARIA DE INFORMACION',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'SUBSECRETARIA DE MEDIOS INSTITUCIONALES',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'SUBSECRETARIA DE PROMOCION DE LA COMUNICACION',
                                ),array(
                                        'id' => '',
                                        'uni_nombre' => 'SUBSECRETARIA NACIONAL DE COMUNICACION',
                                ),
                                
                        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unidades');
    }
}
