<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCantonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cantones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('can_nombre');
            $table->timestamps();
        });

       // DB::table('cantones')->insert(array('id'=>'', 'can_nombre'=>'Quito'));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cantones');
    }
}
